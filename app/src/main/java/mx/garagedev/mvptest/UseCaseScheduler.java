package mx.garagedev.mvptest;

/**
 * MVPTest
 * Created by terry0022 on 25/07/17 - 18:10.
 *
 * Interface for schedulers, see {@link UseCaseThreadPoolScheduler}.
 */
public interface UseCaseScheduler {

    /**
     * ejecucion
     * @param runnable prepara ejecucion
     */
    void execute(Runnable runnable);

    /**
     * Notificacion de respuesta
     * @param response respuesta
     * @param useCaseCallback callback de caso de uso
     * @param <V> datos
     */
    <V extends UseCase.ResponseValue> void notifyResponse(final V response,
                                                          final UseCase.UseCaseCallback<V> useCaseCallback);

    /**
     * En Caso de error
     * @param useCaseCallback callback del caso de uso
     * @param <V> datos
     */
    <V extends UseCase.ResponseValue> void onError(
            final UseCase.UseCaseCallback<V> useCaseCallback);

}
