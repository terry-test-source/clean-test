package mx.garagedev.mvptest.base;

public interface BasePresenter {

    void start();

    void destroy();
}