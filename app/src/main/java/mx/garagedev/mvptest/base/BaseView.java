package mx.garagedev.mvptest.base;

public interface BaseView<T> {

    void setPresenter(T presenter);

}