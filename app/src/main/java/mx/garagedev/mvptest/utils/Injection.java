package mx.garagedev.mvptest.utils;

import android.content.Context;
import android.support.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import mx.garagedev.mvptest.UseCaseHandler;
import mx.garagedev.mvptest.data.source.Repository;
import mx.garagedev.mvptest.data.source.local.LocalDataSource;
import mx.garagedev.mvptest.data.source.remote.RemoteDataSource;
import mx.garagedev.mvptest.login.domain.usecase.LoginProcess;

/**
 * MVPTest
 * Created by terry0022 on 26/07/17 - 00:55.
 */

public class Injection {

    public Injection() {
        //do nothing
    }

    public static LoginProcess provideLoginProcess(@NonNull Context context) {
        Tools.checkNotNull(context);
        return new LoginProcess(Injection.provideTasksRepository(context));
    }

    private static Repository provideTasksRepository(@NotNull Context context) {
        Tools.checkNotNull(context);
        return Repository.getInstance(
                LocalDataSource.getInstance(context),
                RemoteDataSource.getInstance());
    }

    public static UseCaseHandler provideUseCaseHandler() {
        return UseCaseHandler.getInstance();
    }
}
