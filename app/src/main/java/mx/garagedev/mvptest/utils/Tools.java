package mx.garagedev.mvptest.utils;

import android.support.annotation.Nullable;

/**
 * MVPTest
 * Created by terry0022 on 25/07/17 - 18:05.
 */

public class Tools {

    public static final String URL_BASE = "https://www.garagedev.mx/Mitienda/";

    Tools() {
        //do something
    }

    public static <T> T checkNotNull(T reference) {
        if(reference == null) {
            throw new NullPointerException();
        } else {
            return reference;
        }
    }

    public static <T> T checkNotNull(T reference, @Nullable Object errorMessage) {
        if(reference == null) {
            throw new NullPointerException(String.valueOf(errorMessage));
        } else {
            return reference;
        }
    }

}
