package mx.garagedev.mvptest.utils;

import android.util.Log;

import mx.garagedev.mvptest.BuildConfig;

/**
 * MVPTest
 * Created by terry0022 on 26/07/17 - 01:11.
 */

public class MyLogger {

    MyLogger() {
        //do nothing
    }

    public static void i(String tag, String message){
        if (BuildConfig.DEBUG){
            Log.i(tag, message);
        }
    }

    public static void d(String tag, String message){
        if (BuildConfig.DEBUG){
            Log.d(tag, message);
        }
    }

    public static void v(String tag, String message){
        if (BuildConfig.DEBUG){
            Log.v(tag, message);
        }
    }

    public static void e(String tag, String message, Throwable throwable){
        if (BuildConfig.DEBUG){
            Log.e(tag, message, throwable);
        }
    }

    public static void w(String tag, String message, Throwable throwable){
        if (BuildConfig.DEBUG){
            Log.w(tag, message, throwable);
        }
    }
}
