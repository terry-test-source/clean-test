package mx.garagedev.mvptest.data.source;

import mx.garagedev.mvptest.UseCase;
import mx.garagedev.mvptest.login.domain.usecase.LoginProcess;

/**
 * MVPTest
 * Created by terry0022 on 25/07/17 - 17:57.
 */

public interface DataSource {

    void attemptLogin(final LoginProcess.RequestValues requestValues, final UseCase.UseCaseCallback<LoginProcess.ResponseValue> useCaseCallback);

}
