package mx.garagedev.mvptest.data.source.local;

import android.provider.BaseColumns;

/**
 * MVPTest
 * Created by terry0022 on 26/07/17 - 00:34.
 */

final class PersistenceContract {

    PersistenceContract() {
        // do nothing
    }

    /* Inner class that defines the table contents */
    abstract static class DataEntry implements BaseColumns {

        DataEntry() {
            //do nothing
        }

        static final String TABLE_NAME = "data";
        static final String COLUMN_NAME_KEY = "key";
        static final String COLUMN_NAME_VALUE = "value";
    }

}
