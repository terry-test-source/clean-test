package mx.garagedev.mvptest.data.source.remote;

import mx.garagedev.mvptest.login.domain.model.User;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * MVPTest
 * Created by terry0022 on 26/07/17 - 01:22.
 */

public interface DataService {

    /**
     * Proccess to login
     * @param user user
     * @param password password
     * @return UserData item
     */
    @FormUrlEncoded
    @POST("GET/?source=login&source2=validate")
    Call<User> logInProccess(@Field("source1") final String user,
                             @Field("source2") final String password);

}
