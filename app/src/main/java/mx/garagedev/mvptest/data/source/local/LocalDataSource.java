package mx.garagedev.mvptest.data.source.local;

import android.content.Context;
import android.support.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import mx.garagedev.mvptest.UseCase;
import mx.garagedev.mvptest.data.source.DataSource;
import mx.garagedev.mvptest.login.domain.usecase.LoginProcess;
import mx.garagedev.mvptest.utils.MyLogger;
import mx.garagedev.mvptest.utils.Tools;

/**
 * MVPTest
 * Created by terry0022 on 26/07/17 - 00:41.
 */

public class LocalDataSource implements DataSource {

    private static LocalDataSource instance;

    private SQLiteHelper mHelper;

    private LocalDataSource(@NotNull Context context) {
        Tools.checkNotNull(context);
        mHelper = new SQLiteHelper(context);
    }

    public static LocalDataSource getInstance(@NonNull Context context) {
        if (instance == null) {
            instance = new LocalDataSource(context);
        }
        return instance;
    }


    @Override
    public void attemptLogin(LoginProcess.RequestValues requestValues, UseCase.UseCaseCallback<LoginProcess.ResponseValue> useCaseCallback) {
        //do nothing
        MyLogger.i("Local", mHelper.getDatabaseName());
    }
}
