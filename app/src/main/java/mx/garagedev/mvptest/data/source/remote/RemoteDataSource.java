package mx.garagedev.mvptest.data.source.remote;

import mx.garagedev.mvptest.UseCase;
import mx.garagedev.mvptest.data.source.DataSource;
import mx.garagedev.mvptest.login.domain.model.User;
import mx.garagedev.mvptest.login.domain.usecase.LoginProcess;
import mx.garagedev.mvptest.utils.Tools;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * MVPTest
 * Created by terry0022 on 26/07/17 - 00:27.
 */

public class RemoteDataSource implements DataSource {

    private static RemoteDataSource instance;

    RemoteDataSource() {
        //do nothing
    }

    public static DataSource getInstance() {
        if (instance == null) {
            instance = new RemoteDataSource();
        }
        return instance;
    }

    @Override
    public void attemptLogin(final LoginProcess.RequestValues requestValues, final UseCase.UseCaseCallback<LoginProcess.ResponseValue> useCaseCallback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Tools.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        DataService service = retrofit.create(DataService.class);

        final Call<User> requestData = service.logInProccess(requestValues.getMail(), requestValues.getPass());

        requestData.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                LoginProcess.ResponseValue responseValue = new LoginProcess.ResponseValue(response.body());
                useCaseCallback.onSuccess(responseValue);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                useCaseCallback.onError();
            }
        });
    }
}
