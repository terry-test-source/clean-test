package mx.garagedev.mvptest.data.source.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * MVPTest
 * Created by terry0022 on 26/07/17 - 00:29.
 */

public class SQLiteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "MVPTest.db";

    private static final String TEXT_TYPE = " TEXT";

    private static final String SQL_DATA =
            "CREATE TABLE " + PersistenceContract.DataEntry.TABLE_NAME + " (" +
                PersistenceContract.DataEntry.COLUMN_NAME_KEY + TEXT_TYPE +
                PersistenceContract.DataEntry.COLUMN_NAME_VALUE + TEXT_TYPE +
            " )";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_DATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion){
            /* borramos tablas antiguas */
            db.execSQL("DROP TABLE IF EXISTS " + PersistenceContract.DataEntry.TABLE_NAME);

            /* creamos tablas nuevas */
            db.execSQL(SQL_DATA);
        }
    }

}
