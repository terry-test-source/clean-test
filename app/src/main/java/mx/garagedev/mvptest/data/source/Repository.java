package mx.garagedev.mvptest.data.source;

import mx.garagedev.mvptest.UseCase;
import mx.garagedev.mvptest.login.domain.usecase.LoginProcess;

/**
 * MVPTest
 * Created by terry0022 on 25/07/17 - 17:56.
 */

public class Repository implements DataSource {

    private static Repository instance;
    @SuppressWarnings("unused")
    private DataSource mLocalDataSource;
    private DataSource mRemoteDataSource;

    public Repository(DataSource localDataSource, DataSource remoteDataSource) {
        mLocalDataSource = localDataSource;
        this.mRemoteDataSource = remoteDataSource;
    }

    public static Repository getInstance(DataSource localDataSource, DataSource remoteDataSource) {
        if (instance == null){
            instance = new Repository(localDataSource, remoteDataSource);
        }

        return instance;
    }

    @Override
    public void attemptLogin(LoginProcess.RequestValues requestValues, UseCase.UseCaseCallback<LoginProcess.ResponseValue> useCaseCallback) {
        mRemoteDataSource.attemptLogin(requestValues, useCaseCallback);
    }
}
