package mx.garagedev.mvptest.login.domain.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * MVPTest
 * Created by terry0022 on 26/07/17 - 01:08.
 */

public class User {

    /**
     * status : ok
     * pass : b6bc5b88fe8259d8d5a78f01cf22441d198c5166
     * token : 225d27bbe2b8bb173799dcb50804190642902025
     * tiendas : [{"id":"5734f3266ecfc","nombre":"Tienda nueva","articulos_totales":8}]
     * id_tienda : 5734f3266ecfc
     * tipo_usuario : 0
     * session_id : 7cffc4c4c0f48fd1cdf23d4cd0d3d8af
     */

    private String status;
    @SerializedName("pass")
    private String encrypPass;
    private String token;
    @SerializedName("id_tienda")
    private String storeId;
    @SerializedName("tipo_usuario")
    private int userType;
    @SerializedName("session_id")
    private String sessionId;
    @SerializedName("tiendas")
    private List<StoresBean> stores;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEncrypPass() {
        return encrypPass;
    }

    public void setEncrypPass(String encrypPass) {
        this.encrypPass = encrypPass;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public List<StoresBean> getStores() {
        return stores;
    }

    public void setStores(List<StoresBean> stores) {
        this.stores = stores;
    }

    public static class StoresBean {
        /**
         * id : 5734f3266ecfc
         * nombre : Tienda nueva
         * articulos_totales : 8
         */

        private String id;
        @SerializedName("nombre")
        private String name;
        @SerializedName("articulos_totales")
        private int totalArticles;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getTotalArticles() {
            return totalArticles;
        }

        public void setTotalArticles(int totalArticles) {
            this.totalArticles = totalArticles;
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "status='" + status + '\'' +
                ", encrypPass='" + encrypPass + '\'' +
                ", token='" + token + '\'' +
                ", storeId='" + storeId + '\'' +
                ", userType=" + userType +
                ", sessionId='" + sessionId + '\'' +
                ", stores=" + stores +
                '}';
    }
}
