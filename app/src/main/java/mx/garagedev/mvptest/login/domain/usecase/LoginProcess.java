package mx.garagedev.mvptest.login.domain.usecase;

import org.jetbrains.annotations.NotNull;

import mx.garagedev.mvptest.UseCase;
import mx.garagedev.mvptest.data.source.Repository;
import mx.garagedev.mvptest.login.domain.model.User;
import mx.garagedev.mvptest.utils.Tools;

/**
 * MVPTest
 * Created by terry0022 on 26/07/17 - 00:17.
 */

public class LoginProcess extends UseCase<LoginProcess.RequestValues, LoginProcess.ResponseValue> {

    private Repository mRepository;

    public LoginProcess(@NotNull Repository repository) {
        mRepository = Tools.checkNotNull(repository,"Repository cannot be null");
    }

    @Override
    protected void executeUseCase(RequestValues requestValues) {

        mRepository.attemptLogin(requestValues, getUseCaseCallback());

    }

    public static final class RequestValues implements UseCase.RequestValues{

        private String mMail;
        private String mPass;

        public RequestValues(final String mail, final String pass) {
            mMail = Tools.checkNotNull(mail);
            mPass = Tools.checkNotNull(pass);
        }

        public String getMail() {
            return mMail;
        }

        public String getPass() {
            return mPass;
        }
    }

    public static final class ResponseValue implements UseCase.ResponseValue{

        private final User mUser;

        public ResponseValue(User user) {
            this.mUser = Tools.checkNotNull(user);
        }

        public User getUser() {
            return mUser;
        }

    }

}
