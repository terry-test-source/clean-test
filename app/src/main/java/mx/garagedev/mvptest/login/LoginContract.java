package mx.garagedev.mvptest.login;

import mx.garagedev.mvptest.base.BasePresenter;
import mx.garagedev.mvptest.base.BaseView;

/**
 * MVPTest
 * Created by terry0022 on 24/07/17 - 17:21.
 */

public interface LoginContract {

    interface View extends BaseView<Presenter> {

        void showErrorMail(final int source);

        void showErrorPass(final int source);

        void notifySucces();

        void notifyError(String status);

        void loading();
    }

    interface Presenter extends BasePresenter {

        void attemptLogin(final String mail, final String pass);

    }

}
