package mx.garagedev.mvptest.login;

import org.jetbrains.annotations.NotNull;

import mx.garagedev.mvptest.R;
import mx.garagedev.mvptest.UseCase;
import mx.garagedev.mvptest.UseCaseHandler;
import mx.garagedev.mvptest.login.LoginContract.Presenter;
import mx.garagedev.mvptest.login.domain.model.User;
import mx.garagedev.mvptest.login.domain.usecase.LoginProcess;
import mx.garagedev.mvptest.login.domain.usecase.LoginProcess.RequestValues;
import mx.garagedev.mvptest.utils.MyLogger;
import mx.garagedev.mvptest.utils.Tools;

/**
 * MVPTest
 * Created by terry0022 on 24/07/17 - 17:11.
 */
public class LoginActivityPresenterImpl implements Presenter {

    private static final String TAG = "LoginActivityPresenterI";
    private LoginContract.View mView;
    private LoginProcess mLoginProcess;
    private UseCaseHandler mUseCaseHandler;

    LoginActivityPresenterImpl(@NotNull LoginContract.View mView,
                                      @NotNull LoginProcess useCase,
                                      @NotNull UseCaseHandler useCaseHandler) {
        this.mView = Tools.checkNotNull(mView, "View cannnot be null");
        mLoginProcess = useCase;
        mUseCaseHandler = useCaseHandler;
    }

    @Override
    public void attemptLogin(String mail, String pass) {
        if (validate(mail,pass)){

            mView.loading();

            RequestValues requestValues = new RequestValues(mail, pass);
            mUseCaseHandler.execute(mLoginProcess, requestValues, new UseCase.UseCaseCallback<LoginProcess.ResponseValue>() {

                @Override
                public void onSuccess(LoginProcess.ResponseValue response) {
                    User user = response.getUser();

                    processUser(user);
                }

                @Override
                public void onError() {
                    mView.loading();
                    mView.notifyError("Ocurrio un error");
                }

            });
        }
    }

    private void processUser(User user) {
        MyLogger.i(TAG, user.toString());
        mView.loading();
        if ("ok".equals(user.getStatus())){
            mView.notifySucces();
        }else {
            mView.notifyError(user.getStatus());
        }
    }

    private boolean validate(final String mail, final String pass) {
        if (mail.isEmpty()){
            mView.showErrorMail(R.string.error_mail_no_empty);
            return false;
        }

        if (pass.isEmpty()){
            mView.showErrorPass(R.string.error_pass_no_empty);
            return false;
        }

        return true;
    }

    @Override
    public void start() {
        //do something
    }

    @Override
    public void destroy() {
        if (mView != null){
            mView = null;
        }
    }
}
