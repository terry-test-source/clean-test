package mx.garagedev.mvptest.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import mx.garagedev.mvptest.R;
import mx.garagedev.mvptest.utils.Injection;

import static mx.garagedev.mvptest.utils.Tools.checkNotNull;

public class LoginActivity extends AppCompatActivity implements LoginContract.View, View.OnClickListener {

    protected EditText emailInput;
    protected ImageView mailDone;
    protected ImageView mailError;
    protected EditText passwordInput;
    protected ImageView passDone;
    protected ImageView passError;
    protected Button actionSign;
    private LoginContract.Presenter presenter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_login);
        initView();

    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.action_sign) {
            attemptLogin();
        }
    }

    private void attemptLogin() {
        String mail = emailInput.getText().toString();
        String pass = passwordInput.getText().toString();

        presenter.attemptLogin(mail, pass);
    }

    private void initView() {
        /* set Presenter */
        presenter = new LoginActivityPresenterImpl(this,
                Injection.provideLoginProcess(getApplicationContext()),
                Injection.provideUseCaseHandler());

        /* views */
        emailInput = (EditText) findViewById(R.id.email_input);
        mailDone = (ImageView) findViewById(R.id.mail_done);
        mailError = (ImageView) findViewById(R.id.mail_error);
        passwordInput = (EditText) findViewById(R.id.password_input);
        passDone = (ImageView) findViewById(R.id.pass_done);
        passError = (ImageView) findViewById(R.id.pass_error);
        actionSign = (Button) findViewById(R.id.action_sign);
        actionSign.setOnClickListener(LoginActivity.this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle("Cargando");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

    }

    @Override
    public void showErrorMail(int source) {
        emailInput.setError(getString(source));
        mailError.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorPass(int source) {
        passwordInput.setError(getString(source));
        passError.setVisibility(View.VISIBLE);
    }

    @Override
    public void notifySucces() {
        Toast.makeText(getApplicationContext(),"Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void notifyError(String status) {
        Toast.makeText(getApplicationContext(),status, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loading() {
        mailDone.setVisibility(View.VISIBLE);
        mailError.setVisibility(View.GONE);

        passDone.setVisibility(View.VISIBLE);
        passError.setVisibility(View.GONE);

        if (progressDialog.isShowing()){
            progressDialog.dismiss();
        }else{
            progressDialog.show();
        }
    }
}
